#Copyright (c) 2021 zhanghw
#
#Licensed under the Apache License, Version 2.0 (the "License");
#you may not use this file except in compliance with the License.
#You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#Unless required by applicable law or agreed to in writing, software
#distributed under the License is distributed on an "AS IS" BASIS,
#WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#See the License for the specific language governing permissions and
#limitations under the License.

#!/bin/bash

function helpinfo(){
	echo "Usages:"
	echo "./gbox-check.sh            :parse subgit.list"
	echo "./gbox-check.sh help       :show help"
	echo "./gbox-check.sh check      :check subgit branch and log whether same as list on subgit.list"
	echo './gbox-check.sh git "cmd"  :git cmd in all subgit.eg: ./gbox-check.sh git "push --tag"'
	echo 
}

GboxDir="gbox"
SubListFile="$GboxDir""/subgit.list"

helpinfo

if [ "$1" = "help" ];then
	exit
fi

if [ ! -f $SubListFile ];then
	echo "$SubListFile not found"	
	exit
fi

cnt=0
Act=$1
Gitcmd=$2

while read line
do
	cnt=`expr $cnt + 1`
	ProjectPath=`echo $line|grep -o -e "I0=[^|]*"`
	RemoteHost=`echo $line|grep -o -e "I1=[^|]*"`
	URL=`echo $line|grep -o -e "I2=[^|]*"`
	Branch=`echo $line|grep -o -e "I3=[^|]*"`
	CommitInfo=`echo $line|grep -o -e "I4=[^|]*"`

	ProjectPath=${ProjectPath: 3}
	RemoteHost=${RemoteHost: 3}
	URL=${URL: 3}
	Branch=${Branch: 3}
	CommitInfo=${CommitInfo: 3}

        #reduce space
	ProjectPath=`echo $ProjectPath | sed 's/[ \t]*$//g'`
	RemoteHost=`echo $RemoteHost | sed 's/[ \t]*$//g'`
	URL=`echo $URL | sed 's/[ \t]*$//g'`
	Branch=`echo $Branch | sed 's/[ \t]*$//g'`
	CommitInfo=`echo $CommitInfo | sed 's/[ \t]*$//g'`

	#echo "*************************subgit num:$cnt**********************"
	echo -e "\033[32mSubgitNum  : $cnt\033[0m"
	echo -e "\033[32mProjectPath: $ProjectPath\033[0m"
	echo "RemoteHost : $RemoteHost"
	echo "URL        : $URL"
	echo "Branch     : $Branch"
	echo "CommitInfo : $CommitInfo"
	echo ""

	if [ "$Act" = "git" ];then
		cd $ProjectPath >/dev/null
		git $Gitcmd
		cd - >/dev/null
	elif [ "$Act" = "check" ];then
		cd $ProjectPath >/dev/null
		CurBranch=`git branch | awk  '$1 == "*"{print $2}'`
		CurCommitInfo=`git log --pretty=format:"%H,%ad/"`
		CurCommitInfo=${CurCommitInfo%%/*}
		#reduce space
		CurBranch=`echo $CurBranch | sed 's/[ \t]*$//g'`
		CurCommitInfo=`echo $CurCommitInfo | sed 's/[ \t]*$//g'`
		echo "1.Check branch"
		echo "Branch Realtime: $CurBranch"
		echo "Branch Saved   : $Branch"
		if [ "$CurBranch" = "$Branch" ];then
			echo -e "checkbranch: \033[32menjoy\033[0m"
		else
			echo -e "\033[31mcheckbranch: branch not match\033[0m"  
			echo "git info realtime:"
			echo "| I0=$ProjectPath | I1=$RemoteHost | I2=$URL | I3=$CurBranch | I4=$CurCommitInfo |"
		fi	
		echo "2.Check log"
		echo "CommitInfo Realtime: $CurCommitInfo|"	
		echo "CommitInfo Saved   : $CommitInfo|"	
		if [ "$CurCommitInfo" = "$CommitInfo" ];then
			echo -e "checklog  : \033[32menjoy\033[0m"
		else
			echo -e "\033[31mchecklog  : log not match\033[0m"
			echo "git info realtime:"
			echo "| I0=$ProjectPath | I1=$RemoteHost | I2=$URL | I3=$CurBranch | I4=$CurCommitInfo |"
		fi
		echo ""
		cd - >/dev/null
	fi

done < $SubListFile

#str="I0=./app/HubbleCameraService/.git|I1=origin|I2=ssh://git@120.24.45.197:7244/zhanghw/HubbleCameraService.git|I3=master|I4=dc3089ef960d5ce2021cda35726b2edbb14929c1,Wed Jun 9 17:27:42 2021 +0800/"
#I0_URL=`echo $str | grep -o -e "I2=[^|]*"`
#URL=${I0_URL: 3}
#echo "str:$URL"
