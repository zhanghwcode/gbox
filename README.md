Copyright (c) 2021 zhanghw

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

# gbox说明文档

## 一、简介
gbox全称git box，意思是把git仓库当成一个个盒子，大盒子可以容纳多层多个小盒子，小盒子就是subgit，
subgit.list中列举了subgit的信息。父git通过gbox命令集来管理subit,从而获得跟父git配套的子git的分支和
节点等信息。

## 二、部署说明
1. 把gbox拷贝到本地，然后设置环境变量，使gbox命令集可以运行。
2. 获取所有subgit的信息，使用gbox-list.sh可以打印出所有subgit的信息，使用gbox-list.sh save命令，可以把
subgit信息保存到gbox/subgit.list,以备后续使用。
3. 父git提交之前，必须进行如下操作：
(1)使用gbox-check.sh git status查看subgit是否有未提交的改动，如果有，则先提交subgit。
(2)使用gbox-check.sh check检查subgit的信息和subgit.list记录的是否一致，如果不一致会有错误提示。
(3)如果不一致，使用gbox-list.sh saved，或者手动修改subgit.list，必须保证gbox-check.sh check全部通过。

## 三、命令说明
1. 所有gbox命令带help参数，均可以输出帮助信息。
2. gbox-list.sh
功能：获取所有子git的信息
参数：
(1)help  ：打印帮助信息
(2)无参数：打印所有子git的信息，得到的结果可以直接拷贝到gbox/subgit.list。
(3)save  ：保存所有子git的信息到gbox/subgit.list。
3. gbox-check.sh
功能：检查所有子git的信息
参数：
(1)help  ：打印帮助信息
(2)无参数：解析subgit.list，打印结果。
(3)status：进入subgit.list里面所有的subgit，并执行git status。
(4)check ：检查subgit.list里面所有的subgit分支和提交节点跟subgit.list记录是否一致。
(5)git cmd ：后面可以跟完整的git命令，表示对所有subgit执行该git命令。


